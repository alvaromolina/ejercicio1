package com.ucbcba;

public abstract class  Quadrilateral implements Figure {

    protected int width;
    protected int height;
    Quadrilateral(int width){
        this.width = width;
        this.height = width;

    }
    Quadrilateral(int width, int height){
        this.width = width;
        this.height = height;

    }

    @Override
    public double perimeter() {
        return 2*width + 2*height;
    }

    @Override
    public double area() {
        return 0;//TODO
    }


    @Override
    public void draw() {
        //TODO implementar draw de square

    }
}
